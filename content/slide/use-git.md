---
title: 使用 Git
author: He Zhenxing
separator: +++
---

# Git 基本概念

- 版本库 (Repository)
- 工作目录 (Working Directory)
- 缓存区（Staging Area）
- 修改 (Changes)
- 提交 (Commit)
- 推/拉 (Push/Pull)
- 合并 (Merge)
- 标签 (Tag)
- 分支 (Branch)

+++

# Git 的三个基本状态

![Git states](/img/git-states.png)

+++

# Git 基本命令

 - 初始化版本库
   ```sh
   $ git init  # 将当前目录初始化为 git 版本库
   ```

 - 添加文件到缓存区（Staging Area）
   ```sh
   $ git add . # 添加当前目录下及子目录下的文件
   ```

 - 提交修改
   ```sh
   $ git commit -m "Initial commit"
   ```

 - 克隆
   ```sh
   $ git clone git+ssh://hostname/git/myproject
   ```

+++

# Git 基本命令（续）

- 获取修改
   ```sh
   $ git pull
   $ git pull git+ssh://hostname/git/myproject master
   ```

- 合并修改
   ```sh
   $ git merge
   $ git merge git+ssh://hostname/git/myproject master
   ```

- 推送修改
   ```sh
   $ git push origin master
   ```

+++

# Git help 获取帮助

- 使用 man 获取 git 帮助
   ```sh
   $ man git
   ```

- 获取用法和常用命令
   ```sh
   $ git help
   ```

- 获取命令的详细说明
   ```sh
   $ git help add
   ```

+++

# Git add 添加文件

- 添加文档到缓存区（Staging Area）
   ```sh
   $ git add hello.c     # 添加一个文件
   $ git add foo.c bar.c # 添加多个文件
   $ git add src         # 添加目录和目录下的所有文件，包括新文件
   $ git add .           # 添加当前目录下的所有文件，包括新文件
   $ git add --all       # 添加当前目录下的所有文件，包括新文件
   $ git add -u          # 只添加更改的文件，不包括新文件
   ```

- 使用 .gitignore 自动忽略文件
   ```
   *.swp
   ~*
   *.o
   build/*
   ```

+++

# Git status 查看状态

   ```sh
   $ git status
   On branch master
   Changes to be committed:
     (use "git reset HEAD <file>..." to unstage)

   	modified:   conf.py

   Changes not staged for commit:
     (use "git add <file>..." to update what will be committed)
     (use "git checkout -- <file>..." to discard changes in working directory)

   	modified:   local.py

   Untracked files:
     (use "git add <file>..." to include in what will be committed)

   	chinese.style
   ```

+++

# Git log 查看修改日志

   ```sh
   $ git log          # 显示所有修改日志
   $ git log -3       # 显示最近 3 条修改日志
   $ git log --since="1 week ago" # 显示一周之内的修改日志
   $ git log v2.5     # 显示自动v2.5以后的日志
   $ git log src      # 显示修改了 src 目录下文件的修改日志
   ```

+++

# Git tag 添加标签

   ```sh
   $ git tag             # 列出所有的标签
   $ git tag -l 'v2.4.*' # 列出所有的匹配模式的标签
   $ git tag -a v12.3    # 添加 v12.3 的标签标记当前的版本
   $ git tag -d v0.1     # 删除给定的标签
   ```

+++

# Git branch 分支

   ```sh
   $ git branch        # 列出所有的分支，当前分支前面显示 ‘*’ 号
   $ git branch test   # 基于当前版本创建 test 分支
   $ git branch -d test # 删除 test 分支
   $ git checkout test  # 切换到 test 分支
   ```

+++

# Git rebase 操作

假设当前分支是 topic:
   ```
                     A---B---C topic
                    /
               D---E---F---G master
   ```

执行如下操作:
   ```sh
   $ git rebase master
   $ git rebase master topic
   ```

执行后的结果:
   ```
                             A'--B'--C' topic
                            /
               D---E---F---G master
   ```

+++

# Git stash 隐藏修改

- 在不提交本地修改的情况下 pull

   ```sh
   $ git stash               # 将当前的本地修改隐藏起来
   $ git stash list          # 列出隐藏的修改
   $ git pull
   $ git stash pop | apply   # 弹出/应用隐藏的修改
   $ git stash drop          # 删除隐藏的修改
   ```

+++

# 参考资料

- Git Documentation

http://git-scm.com/documentation

- Pro Git

http://git-scm.com/book

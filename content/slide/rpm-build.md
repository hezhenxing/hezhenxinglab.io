---
title: 制作 RPM 软件包
author: He Zhenxing
separator: ===
---

# 为什么使用 RPM 制作软件包
 - 自动化编译制作软件的过程
 - 自动记录安装的文件
 - 支持卸载
 - 编译时依赖
 - 运行时依赖
 - 与其他自动化工具的结合（yum, saltstack, ansible等）

===

# 快速入门
 - 创建 RPM 制作目录结构
    ```sh
    $ mkdir -p ~/rpmbuild/{SPECS,SOURCES}
    ```

 - 把源代码tar包复制到SOURCES的目录
    ```
    $ cp hello-1.0.tar.gz ~/rpmbuild/SOURCES
    ```

 - 编写 SPEC 文件
    ```
    $ vim ~/rpmbuild/SPECS/hello.spec
    ```

 - 执行 rpmbuild
    ```
    $ rpmbuild -ba ~/rpmbuild/SPECS/hello.spec
    ```

===

# 简单 SPEC 文件示例
```
Summary: A short one line description
Name: hello
Version: 1.0
Release: 1
%description
A long multiple line, multiple paragraph description.

%prep
%setup

%build
make
```

===

# 简单 SPEC 文件示例（续）
```
%install
make install DESTDIR=%{buildroot}

%files
%{_bindir}/hello
%doc README

%changelog
* Fri Mar 10 2017 He Zhenxing <zhenxing.he@greatopensource.com>
- First release
```

===

# SPEC 文件概要
 - 软件包基本信息 NVR (Name, Version, Release）
 - 依赖关系 （Requires, BuildRequires, Provides）
 - 编译安装过程 (unpack, build, install)
 - 软件包文件列表
 - 安装和卸载脚本

===

# SPEC 文件 - 基本信息
- Summary
- Name, Version, Release
- URL, License, Group, Packager
- Source (Source0, Source1...)
- Patch (Patch0, Patch1...)
- %description

===

# SPEC 文件 - 基本信息示例

```
Summary: A feature-rich and high scaling distributed database server
Name: dbscale
Version: 1.7
Release: 1
URL: http://www.greatopensource.com
Source: %{name}-%{version}.tar.gz
Group: Applications/Database
%description
The DBScale server is a distributed database server based on sharding
and share-nothing technology. DBScale supports features such as cross
node joining and subquery, distributed transaction, etc.

Please visit our website (http://www.greatopensource.com) for more
information and support.
```

===

# SPEC 文件 - 依赖关系
- BuildRequires 编译软件包时依赖的软件包
```
BuildRequires: gcc-c++ >= 4.8
BuildRequires: cmake >= 2.8.2
BuildRequires: openssl-devel
```

- Requires 安装和运行软件包时依赖的软件包
```
Requires: mysql-client
```

===

# SPEC 文件 - 编译安装过程
- %prep 准备源代码
```
%prep
%setup
```

- %build 配置和编译源代码
```
%configure
make
```

- %install 安装到临时目录下
```
make install DESTDIR=%{buildroot}
```

===

# 对应的手工编译安装过程
 - 准备好软件的源代码
    ```
    $ tar zxf dbscale-1.7.tar.gz
    $ cd dbscale-1.7
    ```

 - 配置和编译软件的源代码
    ```
    $ ./configure --prefix=/usr
    $ make
    ```

 - 把编译好的软件和配置安装到系统中
    ```
    $ make install
    ```

===

# SPEC 文件 - 文件列表
指令 %files 用于定义生成的RPM软件包中包含的文件列表，并可以指定文件的属性：包括
所有者，组和权限，可以使用通配符来添加多个文件，可以使用预定义的宏（如 %{_bindir}）
指定文件路径

```
%files
%defattr(-,root,root)
%{_bindir}/dbscale
%{_libexecdir}/dbscaled
%{_sysconfdir}/dbscale/
%{_mandir}/man1/dbscale*
```

===

# SPEC 文件 - 安装卸载脚本
 - %post 安装软件包文件后执行的脚本
 - %postun 卸载软件包文件后执行的脚本
 - %pre 安装软件包文件之前执行的脚本
 - %preun 卸载软件包文件前执行的脚本

===

# SPEC 文件 - 宏
 - 预定义宏
    - <https://fedoraproject.org/wiki/Packaging:RPMMacros>
 - 使用宏
    ```
    %name
    %{name}
    ```

 - 自定义宏
    ```
    %define mymacro value
    ```

===

# RPM 制作目录

```
$ ls ~/rpmbuild
BUILD  BUILDROOT  RPMS  SOURCES  SPECS  SRPMS
```

 - SPECS 放 SPEC 文件的地方
 - SOURCES 放源代码 tar 包和 patch 文件的地方
 - BUILD 解包和编译源代码的地方
 - BUILDROOT 安装用的临时目录
 - RPMS 保存生成的RPM文件的地方
 - SRPMS 保存生成的SRPM文件的地方

===

# 制作 RPM
 - 使用 SPEC 文件
    ```
    $ rpmbuild -ba dbscale.spec
    ```

 - 使用 tar 包文件，将 SPEC 文件作为原文件打包在源代码tar包内，路径可随意，
   但通常放在 dist 目录下， 可以用 rpmbuild -ta 直接制作RPM包
    ```
    $ rpmbuild -ta dbscale-1.7.tar.gz
    ```

 - 使用 SRPM 重新编译和制作RPM包
    ```
    $ rpmbuild --rebuild dbscale-1.7-1.src.rpm
    ```

===

# 参考资料
 - [RPM 制作指南](http://project.turbolinux.com.cn/documents/14)
 - [Packagers Guide](https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/Packagers_Guide/index.html)
 - [RPM Guide](https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/index.html)

===

# 谢谢！
